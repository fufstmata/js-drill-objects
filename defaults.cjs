const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


function defaults(objectData, defaultData) {
    
    const keyData = Object.keys(objectData);

    for(let index = 0; index < defaultData.length; index++) {
        
        const currentKey = defaultData[index];
        if(keyData.includes(defaultData[index]) === false) {
            objectData[currentKey] = undefined;
        }
    }
}

const defaultData = ['age','wow','random']



module.exports = defaults;