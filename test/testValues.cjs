const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../values.cjs');
const assert = require('assert');


const test = testFunction(testObject);
const expected = [ 'Bruce Wayne', 36, 'Gotham' ];


assert.deepStrictEqual(test,expected);