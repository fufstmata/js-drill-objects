const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../keys.cjs');
const assert = require('assert');

const test = testFunction(testObject);
const expected = [ 'name', 'age', 'location' ];

assert.deepStrictEqual(test,expected);

