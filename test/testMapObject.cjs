const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../mapObject.cjs');
const assert = require('assert');


function callBackFunction(valueData) {
    let test = 'a'
    if (typeof valueData === 'number') {
        let temp = valueData.toString();
        const tempNew = temp.concat('', test);
        return tempNew;
    } else {
        const strNew = valueData.concat('', test);
        return strNew;
    }
}

testFunction(testObject, callBackFunction);
const expected = { name: 'Bruce Waynea', age: '36a', location: 'Gothama' };


assert.deepStrictEqual(testObject,expected);