const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../invert.cjs');
const assert = require('assert');


const test = testFunction(testObject);
const expected = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };

assert.deepStrictEqual(test,expected);