const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../pairs.cjs');
const assert = require('assert');


const test = testFunction(testObject);

const expected = [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ];


assert.deepStrictEqual(test,expected);