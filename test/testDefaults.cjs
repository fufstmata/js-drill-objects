const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testFunction = require('../defaults.cjs');
const assert = require('assert');
const defaultData = ['age','wow','random']

testFunction(testObject, defaultData);

const expected = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    wow: undefined,
    random: undefined
  };

assert.deepStrictEqual(testObject,expected);