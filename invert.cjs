
function invert(objectData) {
    const newCopy = {};
    
    for(let key in objectData) {
        newCopy[objectData[key]] = key; 
    }

    return newCopy;
}

module.exports = invert;