
function pairs(objectData) {
    const pairArray = []
    for (let key in objectData) {
        let temp = [];
        temp.push(key, objectData[key]);
        pairArray.push(temp);
    }

    return pairArray;
}

module.exports = pairs;