
function mapObject(objectData, callBackFunction) {

    for(let key in objectData) {
        objectData[key] = callBackFunction(objectData[key])
    }

}

module.exports = mapObject;
