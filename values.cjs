function values(objectData) {
    let valueArray = []

    for(let keyValue in objectData) {
        valueArray.push(objectData[keyValue]);
    }
    return valueArray;
}

module.exports = values;